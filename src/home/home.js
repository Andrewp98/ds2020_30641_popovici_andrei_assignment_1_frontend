import React, {useContext} from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';
import {LoginContext} from "../utils/LoginContext";
import Doctor from "../components/Doctors";
import Login from "../components/Login";
import Caregiver from "../components/Caregiver";
import Patient from "../components/Patient";
//
// const backgroundStyle = {
//     backgroundPosition: 'center',
//     backgroundSize: 'cover',
//     backgroundRepeat: 'no-repeat',
//     width: "100%",
//     height: "1920px",
//     backgroundImage: `url(${BackgroundImg})`
// };
// const textStyle = {color: 'white',};

const Home = () => {
    const {login} = useContext(LoginContext);

    const getHomeLayout = () => {
        if(login === null)
            return <Login/>
        else if(login.type === "doctor")
            return <Doctor/>
        else if(login.type === "caregiver")
            return <Caregiver/>
        else if(login.type === "patient")
            return <Patient/>;
        return <Login/>;
    }

    return (
        <div>
            {getHomeLayout()}
        </div>
    )
}

export default Home

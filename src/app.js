import React, {useState} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import PersonContainer from './person/person-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import {LoginContext} from "./utils/LoginContext";

const App = () => {
    const [login, setLogin] = useState(null);
    return (
        <LoginContext.Provider value={{login, setLogin}}>
            <div className={styles.back}>
                <Router>
                    <div>
                        <NavigationBar/>
                        <Switch>
                            <Route
                                exact
                                path='/'
                                render={() => <Home/>}
                            />
                            <Route
                                exact
                                path='/person'
                                render={() => <PersonContainer/>}
                            />
                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />
                            <Route render={() => <ErrorPage/>}/>
                        </Switch>
                    </div>
                </Router>
            </div>
        </LoginContext.Provider>
    );
}

export default App

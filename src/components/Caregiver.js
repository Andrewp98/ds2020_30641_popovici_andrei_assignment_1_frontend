import React, {useContext, useEffect, useState} from "react";
import {LoginContext} from "../utils/LoginContext";
import {Button, FormGroup, Input, Label, Modal, ModalHeader, ModalBody, ModalFooter, Card, CardHeader, CardBody} from "reactstrap";
import {HOST} from "../commons/hosts";
import RestApiClient from "../commons/api/rest-client";
import ReactTable from "react-table";
import ShowPatientsOfCaregiver from "./ShowPatientsOfCaregiver";
import SockJsClient from "react-stomp";

const Caregiver = (props) => {
    const {login, setLogin} = useContext(LoginContext);
    const [patients, setPatients] = useState(null);
    const [caregivers, setCaregivers] = useState(null);
    const [selectedCaregiver, setSelectedCaregiver] = useState(null);
    const [isToggled, setIsToggled] = useState(false);
    const [clientRef, setClientRef] = useState(null);
    const [messages, setMessages] = useState([]);

    const onMessageReceived = (message) => {
        if(login.id === message.caregiver_id) {
            console.log("AM PRIMIT", message);
            setMessages([...messages, message]);
        }
    }

    useEffect(() => {
        getPatients();
        getCaregivers();
        setSelectedCaregiver(caregivers);
    }, []);

    const getPatients = () => {
        let request = new Request(HOST.backend_api + "/caregiver/"+login.id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        });
        RestApiClient.performRequest(request, (value) => setPatients(value.patList));
    }

    const getCaregivers = () => {
        let request = new Request(HOST.backend_api + "/caregiver", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        });
        RestApiClient.performRequest(request, (value) => setCaregivers(value));
    }

    const columns = [
        {
            Header: "Name",
            accessor: 'name'
        },
        {
            Header: 'Age',
            accessor: 'age',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: "Medical record",
            accessor: "medRec"
        }
    ]

    return <div style={{display: "flex",
        flexDirection:"column",
        flexWrap:"wrap",
    }}>

        <CardHeader>
            <strong style={{float:"left"}}> Caregiver Dashboard </strong>

            <Button color="primary" onClick={() => setLogin(null)}
                    style={{float:"right"}}>Logout</Button>
        </CardHeader>
        <CardBody>
            {patients && <ReactTable
                data={patients}
                columns={columns}
                defaultPageSize={10}
            />}
        </CardBody>
        {messages.map((message) => {
            return <p>{patients.find((patient) => patient.id === message.patient_id).name} -> {new Date(parseInt(message.startTime)).toString()} -> {new Date(parseInt(message.endTime)).toString()} --------> {message.activity}</p>
        })}
        <SockJsClient url='http://assignment-1-backend.herokuapp.com/sockets' topics={['/alert']}
                      onMessage={onMessageReceived}
                      onConnect={()=>console.log("Connected")}
                      ref={ (client) => { setClientRef(client) }} />
    </div>
};

export default Caregiver;
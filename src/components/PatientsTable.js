import React, {useEffect, useState} from "react";
import {HOST} from "../commons/hosts";
import RestApiClient from "../commons/api/rest-client";
import {Button} from "reactstrap";
import Modal from "reactstrap/es/Modal";
import AddPatientModal from "./AddPatientModal";
import ReactTable from "react-table";

const PatientsTable = ({getPatients, patients}) => {
    const [openModal, setOpenModal] = useState(false);
    const [patientToUpdate, setPatientToUpdate] = useState(null);

    useEffect(() => {
        getPatients();
    }, []);

    const onDeletePatient = (id) => {
        let request = new Request(HOST.backend_api + "/patient", {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({id})
        });
        RestApiClient.performRequest(request, () => getPatients());
    }

    const onUpdatePatient = (patient) => {
        setOpenModal(true);
        setPatientToUpdate(patient);
    }

    const columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Age',
            accessor: 'age',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Medical Record',
            accessor: 'medRec',
        }, {
            Header: 'Update',
            Cell: (item) => {
                return <Button onClick={() => onUpdatePatient(item.original)}>Update</Button>
            }
        },
        {
            Header: 'Delete',
            Cell: (item) => {
                return <Button onClick={() => onDeletePatient(item.original.id)}>Delete</Button>
            }
        }
    ];

    const toggleModal = () => {
        setOpenModal(false);
        setPatientToUpdate(null);
        getPatients();
    }

    return (
        <div style={{width: "100%"}}>
            {patients && <ReactTable
                data={patients}
                columns={columns}
                defaultPageSize={10}
            />}
            <Modal isOpen={openModal} toggle={toggleModal}>
                {openModal && <AddPatientModal toggle={toggleModal}
                                               age={patientToUpdate.age}
                                               id={patientToUpdate.id}
                                               gender={patientToUpdate.gender}
                                               address={patientToUpdate.address}
                                               name={patientToUpdate.name}
                                               links={patientToUpdate.links}
                                               type={patientToUpdate.type}
                                               username={patientToUpdate.username}
                                               password={patientToUpdate.password}
                                               medRec={patientToUpdate.medRec}
                                               edit={true}
                />}
            </Modal>
        </div>
    );
}

export default PatientsTable;
import React, {useContext, useState} from "react";
import {FormGroup, Label, Input, Button} from "reactstrap";
import {HOST} from "../commons/hosts";
import RestApiClient from "../commons/api/rest-client";
import {LoginContext} from "../utils/LoginContext";

const Login = (props) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const {login, setLogin} = useContext(LoginContext);

    const onLoginPressed = () => {
        console.log("username", username);
        console.log("password", password);
        let request = new Request(HOST.backend_api + "/login" , {
            method: 'POST',
            headers : {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({username: username, password: password})
        });

        console.log("URL: " + request.url);

        RestApiClient.performRequest(request, (value) => setLogin(value));
    }

    return (<div>
        <FormGroup>
            <Label for="username">Username</Label>
            <Input type="email" id="username" placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)}/>
        </FormGroup>
        <FormGroup>
            <Label for="password">Password</Label>
            <Input type="password" id="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}/>
        </FormGroup>
        <Button onClick={onLoginPressed}>Login</Button>
    </div>)
}

export default Login;
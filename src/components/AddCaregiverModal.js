import {Button, FormGroup, Input, Label, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import React, {useState} from "react";
import {HOST} from "../commons/hosts";
import RestApiClient from "../commons/api/rest-client";

const AddCaregiverModal = (props) => {
    const [name, setName] = useState(props.name || "");
    const [address, setAddress] = useState(props.address || "");
    const [age, setAge] = useState(props.age || "");
    const [gender, setGender] = useState(props.gender || "");
    const [username, setUsername] = useState(props.username || "");
    const [password, setPassword] = useState(props.password || "");

    const onAddCaregiver = () => {
        let request = new Request(HOST.backend_api + "/caregiver", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name,
                age,
                gender,
                address,
                username,
                password,
            })
        });
        RestApiClient.performRequest(request);
        props.toggle();
    }

    const onUpdateCaregiver = () => {
        let request = new Request(HOST.backend_api + "/caregiver", {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: props.id,
                name,
                age,
                gender,
                address,
                username,
                password,
                type: props.type,
                patList: props.patList
            })
        });
        RestApiClient.performRequest(request);
        props.toggle();
    }

    return (
        <>
            <ModalHeader toggle={props.toggle}>Caregiver actions</ModalHeader>
            <ModalBody>
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input id="name" placeholder="Name" value={name} onChange={(e) => setName(e.target.value)}/>
                </FormGroup>
                <FormGroup>
                    <Label for="adress">Adress</Label>
                    <Input id="adress" placeholder="Adress" value={address}
                           onChange={(e) => setAddress(e.target.value)}/>
                </FormGroup>
                <FormGroup>
                    <Label for="age">Age</Label>
                    <Input id="age" placeholder="Age" value={age} onChange={(e) => setAge(e.target.value)}/>
                </FormGroup>
                <FormGroup>
                    <Label for="gender">Gender</Label>
                    <Input id="gender" placeholder="Gender" value={gender} onChange={(e) => setGender(e.target.value)}/>
                </FormGroup>
                <FormGroup>
                    <Label for="username">Username</Label>
                    <Input type="email" id="username" placeholder="Username" value={username}
                           onChange={(e) => setUsername(e.target.value)}/>
                </FormGroup>
                <FormGroup>
                    <Label for="password">Password</Label>
                    <Input type="password" id="password" placeholder="Password" value={password}
                           onChange={(e) => setPassword(e.target.value)}/>
                </FormGroup>
            </ModalBody>
            <ModalFooter>
                {!props.edit && <Button color="primary" onClick={onAddCaregiver}>Add</Button>}
                {props.edit && <Button color="primary" onClick={onUpdateCaregiver}>Update</Button>}
            </ModalFooter>
        </>)
}

export default AddCaregiverModal;
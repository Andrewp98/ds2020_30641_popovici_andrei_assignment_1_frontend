import React, {useContext, useEffect, useState} from "react";
import {LoginContext} from "../utils/LoginContext";
import {Button, CardBody, CardHeader} from "reactstrap";
import ReactTable from "react-table";
import {HOST} from "../commons/hosts";
import RestApiClient from "../commons/api/rest-client";

const Patient = () => {
    const {setLogin} = useContext(LoginContext);

    const [patients, setPatients] = useState(null);
    useEffect(() => {
        getPatients();
    }, []);

    const getPatients = () => {
        let request = new Request(HOST.backend_api + "/patient", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        });
        RestApiClient.performRequest(request, (value) => setPatients(value));
    }

    const demoData = [
        {
            name:'Paracetamol',
            side_effects:'Lazyness',
            dosage:'One per day',
        },
        {
            name:'Algocalmin',
            side_effects:'Tired',
            dosage:'One every 7 hours',
        }
    ]

    const columns = [
        {
            Header: "Name",
            accessor: 'name'
        },
        {
            Header: 'Side Effects',
            accessor: 'side_effects',
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
        },
    ]

    return <div style={{display: "flex",
        flexDirection:"column",
        flexWrap:"wrap",
    }}>

        <CardHeader>
            <strong style={{float:"left"}}> Patient Dashboard </strong>
            <Button color="primary" onClick={() => setLogin(null)}
                    style={{float:"right"}}>Logout</Button>
        </CardHeader>
        <CardBody>
            {patients && <ReactTable
                style={{
                    textAlign:"center"
                }}
                data={demoData}
                columns={columns}
                defaultPageSize={10}
            />}
        </CardBody>

    </div>
}

export default Patient;
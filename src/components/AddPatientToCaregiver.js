import React from "react";
import Button from "reactstrap/es/Button";
import {ModalBody, ModalHeader} from "reactstrap";
import ReactTable from "react-table";
import {HOST} from "../commons/hosts";
import RestApiClient from "../commons/api/rest-client";

const AddPatientToCaregiver = (props) => {
    const addPatient = (patient) => {
        // console.log("Should select this patient", patient);
        let request = new Request(HOST.backend_api + "/caregiver", {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ...props.caregiver,
                patList: [...props.caregiver.patList, patient]
            })
        });
        RestApiClient.performRequest(request);
        props.toggle();
    }

    const columns = [
        {
            Header: "Name",
            accessor: 'name'
        },
        {
            Header: "Medical record",
            accessor: "medRec"
        },
        {
            Header: "Select",
            Cell: (item) => {
                return <Button onClick={() => addPatient(item.original)}>Select Patient</Button>
            }
        }
    ]


    return (
        <>
            <ModalHeader toggle={props.toggle}>Caregiver actions</ModalHeader>
            <ModalBody>
                <ReactTable
                    data={props.allPatients}
                    columns={columns}
                    defaultPageSize={10}
                />
            </ModalBody>
        </>)
}

export default AddPatientToCaregiver;
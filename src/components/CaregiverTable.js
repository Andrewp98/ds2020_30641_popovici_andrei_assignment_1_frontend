import React, {useEffect, useState} from "react";
import {HOST} from "../commons/hosts";
import RestApiClient from "../commons/api/rest-client";
import {Button} from "reactstrap";
import Modal from "reactstrap/es/Modal";
import AddCaregiverModal from "./AddCaregiverModal";
import ReactTable from "react-table";
import AddPatientToCaregiver from "./AddPatientToCaregiver";
import ShowPatientsOfCaregiver from "./ShowPatientsOfCaregiver";

const CaregiversTable = ({getCaregivers, caregivers, patients}) => {
    const [openModal, setOpenModal] = useState(false);
    const [caregiverToUpdate, setCaregiverToUpdate] = useState(null);
    const [addPatientToCaregiverModal, setAddPatientToCaregiverModal] = useState(false);
    const [selectedCaregiver, setSelectedCaregiver] = useState(null);
    const [removePatientOfCaregiverModal, setRemovePatientOfCaregiverModal] = useState(false);

    useEffect(() => {
        getCaregivers();
    }, []);

    const onDeleteCaregiver = (id) => {
        let request = new Request(HOST.backend_api + "/caregiver", {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({id})
        });
        RestApiClient.performRequest(request, () => getCaregivers());
    }

    const onUpdateCaregiver = (caregiver) => {
        setOpenModal(true);
        setCaregiverToUpdate(caregiver);
    }

    const addPatient = (caregiver) => {
        setSelectedCaregiver(caregiver);
        setAddPatientToCaregiverModal(true);
    }

    const showPatients = (caregiver) => {
        setSelectedCaregiver(caregiver);
        setRemovePatientOfCaregiverModal(true);
    }

    const toggleAddPatientToCaregiverModal = () => {
        setSelectedCaregiver(null);
        setAddPatientToCaregiverModal(false);
        getCaregivers();
    }

    const toggleRemovePatientOfCaregiverModal = () => {
        setSelectedCaregiver(null);
        setRemovePatientOfCaregiverModal(false);
        getCaregivers();
    }


    const columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Age',
            accessor: 'age',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Add patient',
            Cell: (item) => {
                return <Button onClick={() => addPatient(item.original)}>Add Patient</Button>
            }
        },
        {
            Header: 'Show Patients',
            Cell: (item) => {
                return <Button onClick={() => showPatients(item.original)}>Show Patients</Button>
            }
        },
        {
            Header: 'Update',
            Cell: (item) => {
                return <Button onClick={() => onUpdateCaregiver(item.original)}>Update</Button>
            }
        },
        {
            Header: 'Delete',
            Cell: (item) => {
                return <Button onClick={() => onDeleteCaregiver(item.original.id)}>Delete</Button>
            }
        }
    ];

    const toggleModal = () => {
        setOpenModal(false);
        setCaregiverToUpdate(null);
        getCaregivers();
    }

    return (
        <div style={{width: "100%"}}>
            {caregivers && <ReactTable
                data={caregivers}
                columns={columns}
                defaultPageSize={10}
            />}
            <Modal isOpen={openModal} toggle={toggleModal}>
                {openModal && <AddCaregiverModal toggle={toggleModal}
                                                 age={caregiverToUpdate.age}
                                                 id={caregiverToUpdate.id}
                                                 gender={caregiverToUpdate.gender}
                                                 address={caregiverToUpdate.address}
                                                 name={caregiverToUpdate.name}
                                                 links={caregiverToUpdate.links}
                                                 type={caregiverToUpdate.type}
                                                 username={caregiverToUpdate.username}
                                                 password={caregiverToUpdate.password}
                                                 patList={caregiverToUpdate.patList}
                                                 edit={true}
                />}
            </Modal>
            <Modal isOpen={addPatientToCaregiverModal} toggle={toggleAddPatientToCaregiverModal}>
                {addPatientToCaregiverModal && <AddPatientToCaregiver toggle={toggleAddPatientToCaregiverModal}
                                                                      caregiver={selectedCaregiver}
                                                                      allPatients={patients}
                />}
            </Modal>
            <Modal isOpen={removePatientOfCaregiverModal} toggle={toggleRemovePatientOfCaregiverModal}>
                {removePatientOfCaregiverModal && <ShowPatientsOfCaregiver toggle={toggleRemovePatientOfCaregiverModal}
                                                                      caregiver={selectedCaregiver}
                                                                      patients={selectedCaregiver.patList}
                />}
            </Modal>
        </div>
    );
}

export default CaregiversTable;
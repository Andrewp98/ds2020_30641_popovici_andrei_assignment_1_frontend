import React, {useContext, useState} from "react";
import {LoginContext} from "../utils/LoginContext";
import {Button, CardHeader, Col, Modal, Row} from "reactstrap";
import CardBody from "reactstrap/es/CardBody";
import AddPatientModal from "./AddPatientModal";
import PatientsTable from "./PatientsTable";
import {HOST} from "../commons/hosts";
import RestApiClient from "../commons/api/rest-client";
import CaregiversTable from "./CaregiverTable";
import AddCaregiverModal from "./AddCaregiverModal";
import AddDoctorModal from "./AddDoctorModal";

const Doctor = () => {
    const {setLogin} = useContext(LoginContext);

    // const [patient_list, setPatient_list] = useState("");
    const [patients, setPatients] = useState(null);
    const [caregivers, setCaregivers] = useState(null);

    const getPatients = () => {
        let request = new Request(HOST.backend_api + "/patient", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        });
        RestApiClient.performRequest(request, (value) => setPatients(value));
    }

    const getCaregivers = () => {
        let request = new Request(HOST.backend_api + "/caregiver", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        });
        RestApiClient.performRequest(request, (value) => setCaregivers(value));
    }

    const [modal, setModal] = useState(false);
    const toggle = () => {
        setModal(!modal);
        getPatients();
        getCaregivers();
    }

    const [modal1, setModal1] = useState(false);
    const toggle1 = () => {
        setModal1(!modal1);
        getPatients();
        getCaregivers();
    }

    const [modal2, setModal2] = useState(false);
    const toggle2 = () => {
        setModal2(!modal2);
        getPatients();
        getCaregivers();
    }

    return <div style={{display: "flex",
                        flexDirection:"column",
                        flexWrap:"wrap",
                        }}>
        <CardHeader>
            <strong style={{float:"left"}}> Doctor Dashboard </strong>
            <Button color="primary" onClick={() => setLogin(null)}
                    style={{float:"right"}}>Logout</Button>
        </CardHeader>
        <CardBody>

            <br/>
            <Row>
                <Col sm={{size: '8', offset: 1}}>
                    <Button color="primary" onClick={() => toggle2()}>Doctor</Button>
                    &nbsp;
                    <Button color="primary" onClick={() => toggle()}>Patient</Button>
                    &nbsp;
                    <Button color="primary" onClick={() => toggle1()}>Caregiver</Button>
                </Col>
            </Row>
            <br/>
            <Row>
                <PatientsTable getPatients={getPatients} patients={patients}/>
                {/*<PersonTable tableData={tableData}/>*/}
            </Row>
            <br/>
            <Row>
                <CaregiversTable getCaregivers={getCaregivers} caregivers={caregivers} patients={patients}/>
            </Row>


            {/* <!-- primul modal --> */}
            <Modal isOpen={modal} toggle={toggle}>
                <AddPatientModal toggle={toggle}/>
            </Modal>

            <Modal isOpen={modal1} toggle={toggle1}>
                <AddCaregiverModal toggle={toggle1}/>
            </Modal>

            <Modal isOpen={modal2} toggle={toggle2}>
                <AddDoctorModal toggle={toggle2}/>
            </Modal>
        </CardBody>

    </div>
}

export default Doctor;
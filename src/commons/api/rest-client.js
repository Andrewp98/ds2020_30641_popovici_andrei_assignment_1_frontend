function performRequest(request, callback){
    fetch(request)
        .then(
            function(response) {
                if (response.ok) {
                    try {
                        response.text().then(text => {
                            let val;
                            try {
                                val = JSON.parse(text);
                            } catch {
                                val = text;
                            }
                            callback && callback(val, response.status, null)
                        });
                    }
                    catch {
                        callback && callback(response.body, response.status, null);
                    }
                }
                else {
                    // try {
                    //     response.json().then(err => callback(null, response.status,  err));
                    // }
                    // catch {
                        callback && callback(response.body, response.status, null);
                    // }
                }
            })
        .catch(function (err) {
            //catch any other unexpected error, and set custom code for error = 1
            callback && callback(null, 1, err)
        });
}

module.exports = {
    performRequest
};
